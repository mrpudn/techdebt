local json = require "dkjson"
local cjson = require "cjson"

function love.load()
  love.window.setMode(360, 360, { resizable = true })
end

function love.draw()
  love.graphics.print(json.encode({ hello = "World" }), 20, 20)
  love.graphics.print(cjson.encode({ hello = "C World" }), 20, 40)
end

