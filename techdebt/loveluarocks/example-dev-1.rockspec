package = "example"
version = "dev-1"
source = {
   url = "*** please add URL for source tarball, zip or repository here ***"
}
description = {
   homepage = "*** please enter a project homepage ***",
   license = "*** please specify a license ***"
}
dependencies = {
  "lua ~> 5.1",
  "dkjson ~> 2.7",
  "lua-cjson ~> 2.1"
}
build = {
   type = "builtin",
   modules = {}
}
